﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public interface IUserMediaRepository
    {
        ICollection<UserMedia> GetMediaForUser(int userId);
        ICollection<UserMedia> GetUsersForMedia(int mediaTypeId);
        UserMedia GetUserMedia(int userId, int mediaTypeId);
        UserMedia CreateUserMedia(UserMedia userMedia);
    }
}
