﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class UserTypeRepository : BaseRepository, IUserTypeRepository
    {
        public ICollection<UserType> GetUserTypes()
        {
            using (var context = GetContext())
            {
                var userTypes = context.UserTypes.ToList();
                return userTypes;
            }
        }

        public UserType GetUserType(int userTypeId)
        {
            using (var context = GetContext())
            {
                var userType = context.UserTypes.FirstOrDefault(u => u.UserTypeId == userTypeId);
                return userType;
            }
        }
    }
}
