﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public interface IUserTypeRepository
    {
        ICollection<UserType> GetUserTypes();
        UserType GetUserType(int userTypeId);
    }
}
