﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class MediaRepository : BaseRepository, IMediaRepository
    {
        public IEnumerable<Media> GetMediaTypes()
        {
            using (var context = GetContext())
            {
                var mediaTypes = context.Media.ToList();
                return mediaTypes;
            }
        }

        public Media GetMediaType(int mediaTypeId)
        {
            using (var context = GetContext())
            {
                var media = context.Media.FirstOrDefault(m => m.MediaTypeId == mediaTypeId);
                return media;
            }
        }
    }
}
