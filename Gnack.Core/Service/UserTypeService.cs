﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;
using Gnack.Core.Repository;

namespace Gnack.Core.Service
{
    class UserTypeService : IUserTypeService
    {
        private readonly IUserTypeRepository _userTypeRepository = new UserTypeRepository();

        public ICollection<UserType> GetUserTypes()
        {
           return _userTypeRepository.GetUserTypes();
        }

        public UserType GetUserType(int userTypeId)
        {
            return _userTypeRepository.GetUserType(userTypeId);
        }
    }
}
