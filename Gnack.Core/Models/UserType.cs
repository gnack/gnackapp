using System;
using System.Collections.Generic;

namespace Gnack.Core.Models
{
    public partial class UserType
    {
        public int UserTypeId { get; set; }
        public string Description { get; set; }
    }
}
