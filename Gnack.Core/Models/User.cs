using System;
using System.Collections.Generic;

namespace Gnack.Core.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public int UserTypeId { get; set; }
    }
}
