﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class LeadRepository : BaseRepository, ILeadRepository
    {
        public Lead CreateLead(int leadtype, string company, string email, string leadname)
        {
            //using (var connection = new SqlConnection("Server=tcp:gnackserver.database.windows.net,1433;Database=GnackWeb_db;User ID=dboadmin@gnackserver;Password={Gnackadmin1};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"))
            //{
            //    connection.Open();

            //    var sql =
            //        "INSERT INTO Lead(LeadName, Company, EmailAddress, LeadType) VALUES(@leadname, @company, @email,@leadtype)";
            //    using (var cmd = new SqlCommand(sql, connection))
            //    {
            //        cmd.Parameters.AddWithValue("@leadname", leadname);
            //        cmd.Parameters.AddWithValue("@company", company);
            //        cmd.Parameters.AddWithValue("@email", email);
            //        cmd.Parameters.AddWithValue("@leadtype", leadtype);

            //        cmd.ExecuteNonQuery();
            //    }
            //}
            //return lead;


            using (var context = GetContext())
            {
                var lead = new Lead { LeadType = leadtype, Company = company, EmailAddress = email, LeadName = leadname };

                //var existingLead = context.Leads.Firs(u => u.EmailAddress == lead.EmailAddress);

                //context.Entry(existingLead).CurrentValues.SetValues(lead);
                context.Leads.Add(lead);

                try
                {
                    context.SaveChanges();

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = dbEx;
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string message = string.Format("{0}:{1}",
                                validationErrors.Entry.Entity.ToString(),
                                validationError.ErrorMessage);
                            // raise a new exception nesting  
                            // the current instance as InnerException  
                            raise = new InvalidOperationException(message, raise);
                        }
                    }
                    throw raise;
                }
                return lead;
            }
        }
    }
}
