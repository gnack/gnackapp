﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public ICollection<User> GetUsers()
        {
            using (var context = GetContext())
            {
                var users = context.Users.ToList();
                return users;
            }
        }

        public User CreateUser(User user)
        {
            using (var context = GetContext())
            {
                var existingUser = context.Users.FirstOrDefault(u => u.UserEmail == user.UserEmail);

                context.Entry(existingUser).CurrentValues.SetValues(user);

                if (existingUser == null)
                {
                    context.Users.Add(user);
                }
                context.SaveChanges();
                return user;
            }
        }
    }
}
