﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gnack.Core.Models
{
    public class Lead
    {
        public int LeadId { get; set; }
        public int LeadType { get; set; }
        public string LeadName { get; set; }
        public string EmailAddress { get; set; }
        public string Company { get; set; }
    }
}
