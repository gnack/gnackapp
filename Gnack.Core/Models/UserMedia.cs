using System;
using System.Collections.Generic;

namespace Gnack.Core.Models
{
    public partial class UserMedia
    {
        public int MediaTypeId { get; set; }
        public string UserMediaName { get; set; }
        public int UserId { get; set; }
    }
}
