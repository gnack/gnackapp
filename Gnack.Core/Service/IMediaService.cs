﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Service
{
    public interface IMediaService
    {
        IEnumerable<Media> GetMediaTypes();
        Media GetMediaType(int mediaTypeId);
    }
}
