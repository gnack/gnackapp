using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Gnack.Core.Models.Mapping;

namespace Gnack.Core.Models
{
    public partial class GnackWeb_dbContext : DbContext
    {
        static GnackWeb_dbContext()
        {
            Database.SetInitializer<GnackWeb_dbContext>(null);
        }

        public GnackWeb_dbContext()
            : base("Server=tcp:gnackserver.database.windows.net,1433;Database=GnackWeb_db;User ID=dboadmin@gnackserver;Password=Gnackadmin1;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;")
        {
            //Configuration.LazyLoadingEnabled = false;
            //Configuration.ProxyCreationEnabled = false;

            Database.SetInitializer<GnackWeb_dbContext>(null);
        }

        public DbSet<Media> Media { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserMedia> UserMedias { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Lead> Leads { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MediaMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserMediaMap());
            modelBuilder.Configurations.Add(new UserTypeMap());
            modelBuilder.Configurations.Add(new LeadMap());
        }
    }
}
