﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using Gnack.Core.Models;
using Gnack.Core.Service;
using Microsoft.Ajax.Utilities;

namespace Gnack.Web.Controllers
{
    public class HomeController : Controller
    {
        private Gnack.Core.Service.ILeadService _service = new LeadService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Get in Touch.";

            return View();
        }

        public ActionResult SendInfluencer(string email, string name)
        {
            _service.CreateLead(0, "", email, name);
            return RedirectToAction("Index");
        }

        public ActionResult SendCompany(string from, string contactname, string message, string company)
        {

            _service.CreateLead(1, company, from, contactname);
            return RedirectToAction("About");
        }

        public ActionResult TOS()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }
    }
}