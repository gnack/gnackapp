﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class UserMediaRepository : BaseRepository, IUserMediaRepository
    {
        public ICollection<UserMedia> GetMediaForUser(int userId)
        {
            using (var context = GetContext())
            {
                var userMedias = context.UserMedias.Where(um => um.UserId == userId).ToList();
                return userMedias;
            }
        }

        public ICollection<UserMedia> GetUsersForMedia(int mediaTypeId)
        {
            using (var context = GetContext())
            {
                var userMedias = context.UserMedias.Where(um => um.MediaTypeId == mediaTypeId).ToList();
                return userMedias;
            }
        }

        public UserMedia GetUserMedia(int userId, int mediaTypeId)
        {
            using (var context = GetContext())
            {
                var usermedia =
                    context.UserMedias.FirstOrDefault(um => um.UserId == userId && um.MediaTypeId == mediaTypeId);
                return usermedia;
            }
        }

        public UserMedia CreateUserMedia(UserMedia userMedia)
        {
            using (var context = GetContext())
            {
                var existingUser = context.UserMedias.FirstOrDefault(um => um.UserId == userMedia.UserId && um.MediaTypeId == userMedia.MediaTypeId);

                context.Entry(existingUser).CurrentValues.SetValues(userMedia);

                if (existingUser == null)
                {
                    context.UserMedias.Add(userMedia);
                }
                context.SaveChanges();
                return userMedia;
            }
        }
    }
}
