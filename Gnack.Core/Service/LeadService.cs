﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;
using Gnack.Core.Repository;

namespace Gnack.Core.Service
{
    public class LeadService : ILeadService
    {
        private readonly ILeadRepository _leadRepository = new LeadRepository();

        public Lead CreateLead(int leadtype, string company, string email, string name)
        {
            if (leadtype == null || company == null || email == null || name == null)
            {
                return null;
            }
            return _leadRepository.CreateLead(leadtype, company, email, name);
        }
    }
}
