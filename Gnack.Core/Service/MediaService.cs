﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;
using Gnack.Core.Repository;

namespace Gnack.Core.Service
{
    public class MediaService : IMediaService
    {
        private readonly IMediaRepository _mediaRepository = new MediaRepository();

        public IEnumerable<Media> GetMediaTypes()
        {
            return _mediaRepository.GetMediaTypes();
        }

        public Media GetMediaType(int mediaTypeId)
        {
            return _mediaRepository.GetMediaType(mediaTypeId);
        }
    }
}
