using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Gnack.Core.Models.Mapping
{
    public class UserMediaMap : EntityTypeConfiguration<UserMedia>
    {
        public UserMediaMap()
        {
            // Primary Key
            this.HasKey(t => new { t.MediaTypeId, t.UserId });

            // Properties
            this.Property(t => t.MediaTypeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.UserMediaName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("UserMedia");
            this.Property(t => t.MediaTypeId).HasColumnName("MediaTypeId");
            this.Property(t => t.UserMediaName).HasColumnName("UserMediaName");
            this.Property(t => t.UserId).HasColumnName("UserId");
        }
    }
}
