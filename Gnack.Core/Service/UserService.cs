﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;
using Gnack.Core.Repository;

namespace Gnack.Core.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository = new UserRepository();

        public ICollection<User> GetUsers()
        {
            return _userRepository.GetUsers();
        }

        public User CreateUser(User user)
        {
            return _userRepository.CreateUser(user);
        }
    }
}
