using System;
using System.Collections.Generic;

namespace Gnack.Core.Models
{
    public partial class Media
    {
        public int MediaTypeId { get; set; }
        public string Description { get; set; }
    }
}
