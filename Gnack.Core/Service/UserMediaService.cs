﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;
using Gnack.Core.Repository;

namespace Gnack.Core.Service
{
    public class UserMediaService : IUserMediaService
    {
        private readonly IUserMediaRepository _userMediaRepository = new UserMediaRepository();

        public ICollection<UserMedia> GetMediaForUser(int userId)
        {
            return _userMediaRepository.GetMediaForUser(userId);
        }

        public ICollection<UserMedia> GetUsersForMedia(int mediaTypeId)
        {
            return _userMediaRepository.GetUsersForMedia(mediaTypeId);
        }

        public UserMedia GetUserMedia(int userId, int mediaTypeId)
        {
            return _userMediaRepository.GetUserMedia(userId, mediaTypeId);
        }

        public UserMedia CreateUserMedia(UserMedia userMedia)
        {
            return _userMediaRepository.CreateUserMedia(userMedia);
        }
    }
}
