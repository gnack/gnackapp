﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Repository
{
    public class BaseRepository
    {
        public GnackWeb_dbContext GetContext()
        {
            return new GnackWeb_dbContext();
        }
    }
}
