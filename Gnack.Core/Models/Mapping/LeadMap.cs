﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gnack.Core.Models.Mapping
{
    public class LeadMap : EntityTypeConfiguration<Lead>
    {
        public LeadMap()
        {
            // Primary Key
            this.HasKey(t => t.LeadId);

            // Properties
            this.Property(t => t.LeadType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Company)
                .IsRequired()
                .HasMaxLength(50);
            this.Property(t => t.LeadName)
                .IsRequired()
                .HasMaxLength(50);
            this.Property(t => t.EmailAddress)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Lead");
            this.Property(t => t.LeadId).HasColumnName("LeadId");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            this.Property(t => t.LeadName).HasColumnName("LeadName");
            this.Property(t => t.LeadType).HasColumnName("LeadType");
        }
    }
}
