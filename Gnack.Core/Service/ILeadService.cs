﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gnack.Core.Models;

namespace Gnack.Core.Service
{
    public interface ILeadService
    {
        Lead CreateLead(int leadtype, string company, string email, string name);
    }
}
